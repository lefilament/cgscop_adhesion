.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


================================
CG SCOP - Processus d'adhésion
================================

Modifie le module Contact:
    - ajout d'un onglet dans la fiche Organisme avec le formulaire d'adhésion
    - ajout de nouveaux champs dans res.partner pour le process d'adhésion

Credits
=======

Funders
------------

The development of this module has been financially supported by:

    Confédération Générale des SCOP (https://www.les-scop.coop)


Contributors
------------

* Juliana Poudou <juliana@le-filament.com>
* Rémi Cazenave <remi@le-filament.com>
* Benjamin Rivier <benjamin@le-filament.com>


Maintainer
----------

.. image:: https://le-filament.com/images/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament
