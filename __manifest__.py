{
    "name": "CG SCOP - Processus d'adhésion",
    "summary": "CG SCOP - Processus d'adhésion",
    "version": "12.0.1.0.1",
    "development_status": "Beta",
    "author": "Le Filament",
    "maintainers": ["remi-filament"],
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": [
        "cgscop_partner",
    ],
    "data": [
        "security/ir.model.access.csv",
        "views/assets.xml",
        "views/res_partner.xml",
    ]
}
