# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields, api


class ScopPartner(models.Model):
    _inherit = "res.partner"

    # Processus d'adhésion
    percent_realisation = fields.Selection(
        [(0, 0),
         (20, 20),
         (40, 40),
         (60, 60),
         (80, 80),
         (100, 100)],
        string='Probabilité de réalisation')
    date_realisation = fields.Date("Date prévue de réalisation")
    prescriber_canal_id = fields.Many2one(
        'res.partner.prescriber.canal',
        string='Canal de Prescription',
        on_delete='restrict')

    date_first_rdv = fields.Date("Date du 1er rdv")
    date_send_guide = fields.Date("Date d’envoi du guide de faisabilité")
    staff_existing = fields.Integer("Effectif connu")
    staff_planned = fields.Integer("Effectif prévu")
    adhesion_comments = fields.Text("Commentaires Adhésion")

    feasibility_study = fields.Boolean("Etude de faisabilité signée / validée")
    amount_feasibility_study = fields.Integer(
        "Montant de l’étude de faisabilité")
    date_prediag = fields.Date("Date de pré-diagnostic")
    date_convention = fields.Date("Date de signature de la convention")
    amount_convention = fields.Integer("Montant de la convention")
    file_full = fields.Boolean("Dossier d’adhésion complet")
    date_transmission_cg = fields.Date(
        "Date de transmission du dossier à la CG")

    # Dossier UE
    file_fse_open = fields.Boolean("Dossier FSE ouvert Oui/Non")
    date_return_file = fields.Date("Date de retour du dossier")
    fse_full = fields.Boolean("FSE Complet")
    recipient_file_fse = fields.Many2one(
        'res.partner',
        string="Destinataire du dossier FSE")

    @api.multi
    def scop_send_to_cg(self):
        """ Hérite la fonction d'envoi de l'organisme à la CG
        pour validation afin de positionner la date_transmission_cg avec
        la date du jour

        @return : True
        """
        super(ScopPartner, self).scop_send_to_cg()
        self.date_transmission_cg = fields.Date.today()
        return True


class ResPartnerPrescriberCanal(models.Model):
    _name = "res.partner.prescriber.canal"
    _description = "Canal de Prescription"

    name = fields.Char('Canal de Prescription')
