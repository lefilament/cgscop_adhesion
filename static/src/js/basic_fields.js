odoo.define('cgscop_adhesion.basic_fields', function (require) {
"use strict";

var AbstractField = require('web.AbstractField');
var fieldRegistry = require('web.field_registry');

var PriorityWidgetCG = AbstractField.extend({
    className: "o_priority",
    events: {
        'click > a': '_onClick',
    },
    supportedFieldTypes: ['selection'],

    isSet: function () {
        return true;
    },

    _render: function () {
        var self = this;
        var index_value = this.value ? _.findIndex(this.field.selection, function (v) {
            return v[0] === self.value;
        }) : 0;
        this.$el.empty();
        this.empty_value = this.field.selection[0][0];
        _.each(this.field.selection.slice(1), function (choice, index) {
            self.$el.append(self._renderStar('<a href="#">', index_value >= index+1, index+1, choice[1]));
        });
    },

    _renderStar: function (tag, isFull, index, tip) {
        return $(tag)
            .attr('title', tip)
            .attr('aria-label', tip)
            .attr('data-index', index)
            .addClass('o_priority_star fa')
            .toggleClass('fa-star', isFull)
            .toggleClass('fa-star-o', !isFull);
    },

    _onClick: function (event) {
        event.preventDefault();
        event.stopPropagation();
    },

});

fieldRegistry.add('prioritycg', PriorityWidgetCG);

});
